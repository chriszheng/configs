# To the extent possible under law, the author(s) have dedicated all 
# copyright and related and neighboring rights to this software to the 
# public domain worldwide. This software is distributed without any warranty. 
# You should have received a copy of the CC0 Public Domain Dedication along 
# with this software. 
# If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 

# base-files version 4.1-1

# ~/.bashrc: executed by bash(1) for interactive shells.

# The latest version as installed by the Cygwin Setup program can
# always be found at /etc/defaults/etc/skel/.bashrc

# Modifying /etc/skel/.bashrc directly will prevent
# setup from updating it.

# The copy in your home directory (~/.bashrc) is yours, please
# feel free to customise it to create a shell
# environment to your liking.  If you feel a change
# would be benifitial to all, please feel free to send
# a patch to the cygwin mailing list.

# User dependent .bashrc file

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# Shell Options
#
# See man bash for more options...
#
# Don't wait for job termination notification
set -o notify
#
# Don't use ^D to exit
# set -o ignoreeof
#
# Use case-insensitive filename globbing
# shopt -s nocaseglob
#
# Make bash append rather than overwrite the history on disk
# shopt -s histappend
#
# When changing directory small typos can be ignored by bash
# for example, cd /vr/lgo/apaache would find /var/log/apache
# shopt -s cdspell

# Completion options
#
# These completion tuning parameters change the default behavior of bash_completion:
#
# Define to access remotely checked-out files over passwordless ssh for CVS
# COMP_CVS_REMOTE=1
#
# Define to avoid stripping description in --option=description of './configure --help'
# COMP_CONFIGURE_HINTS=1
#
# Define to avoid flattening internal contents of tar files
# COMP_TAR_INTERNAL_PATHS=1
#
# Uncomment to turn on programmable completion enhancements.
# Any completions you add in ~/.bash_completion are sourced last.
# [[ -f /etc/bash_completion ]] && . /etc/bash_completion

# History Options
#
# Don't put duplicate lines in the history.
# export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
#
# Ignore some controlling instructions
# HISTIGNORE is a colon-delimited list of patterns which should be excluded.
# The '&' is a special pattern which suppresses duplicate entries.
# export HISTIGNORE=$'[ \t]*:&:[fb]g:exit'
# export HISTIGNORE=$'[ \t]*:&:[fb]g:exit:ls' # Ignore the ls command as well
#
# Whenever displaying the prompt, write the previous line to disk
# export PROMPT_COMMAND="history -a"

# Aliases
#
# Some people use a different file for aliases
# if [ -f "${HOME}/.bash_aliases" ]; then
#   source "${HOME}/.bash_aliases"
# fi
#
# Some example alias instructions
# If these are enabled they will be used instead of any instructions
# they may mask.  For example, alias rm='rm -i' will mask the rm
# application.  To override the alias instruction use a \ before, ie
# \rm will call the real rm not the alias.
#
# Interactive operation...
# alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'
#
# Default to human readable figures
# alias df='df -h'
# alias du='du -h'
#
# Misc :)
# alias less='less -r'                          # raw control characters
# alias whence='type -a'                        # where, of a sort
alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
#
# Some shortcuts for different directory listings
# alias ls='ls -hF --color'                     # classify files in colour
alias ls='ls --color=auto'
# alias dir='ls --color=auto --format=vertical'
# alias vdir='ls --color=auto --format=long'
# alias ll='ls -l'                              # long list
# alias la='ls -A'                              # all but . and ..
# alias l='ls -CF'                              #

# Umask
#
# /etc/profile sets 022, removing write perms to group + others.
# Set a more restrictive umask: i.e. no exec perms for others:
# umask 027
# Paranoid: neither group nor others have any perms:
# umask 077

# Functions
#
# Some people use a different file for functions
# if [ -f "${HOME}/.bash_functions" ]; then
#   source "${HOME}/.bash_functions"
# fi
#
# Some example functions:
#
# a) function settitle
# settitle () 
# { 
#   echo -ne "\e]2;$@\a\e]1;$@\a"; 
# }
# 
# b) function cd_func
# This function defines a 'cd' replacement function capable of keeping, 
# displaying and accessing history of visited directories, up to 10 entries.
# To use it, uncomment it, source this file and try 'cd --'.
# acd_func 1.0.5, 10-nov-2004
# Petar Marinov, http:/geocities.com/h2428, this is public domain
# cd_func ()
# {
#   local x2 the_new_dir adir index
#   local -i cnt
# 
#   if [[ $1 ==  "--" ]]; then
#     dirs -v
#     return 0
#   fi
# 
#   the_new_dir=$1
#   [[ -z $1 ]] && the_new_dir=$HOME
# 
#   if [[ ${the_new_dir:0:1} == '-' ]]; then
#     #
#     # Extract dir N from dirs
#     index=${the_new_dir:1}
#     [[ -z $index ]] && index=1
#     adir=$(dirs +$index)
#     [[ -z $adir ]] && return 1
#     the_new_dir=$adir
#   fi
# 
#   #
#   # '~' has to be substituted by ${HOME}
#   [[ ${the_new_dir:0:1} == '~' ]] && the_new_dir="${HOME}${the_new_dir:1}"
# 
#   #
#   # Now change to the new dir and add to the top of the stack
#   pushd "${the_new_dir}" > /dev/null
#   [[ $? -ne 0 ]] && return 1
#   the_new_dir=$(pwd)
# 
#   #
#   # Trim down everything beyond 11th entry
#   popd -n +11 2>/dev/null 1>/dev/null
# 
#   #
#   # Remove any other occurence of this dir, skipping the top of the stack
#   for ((cnt=1; cnt <= 10; cnt++)); do
#     x2=$(dirs +${cnt} 2>/dev/null)
#     [[ $? -ne 0 ]] && return 0
#     [[ ${x2:0:1} == '~' ]] && x2="${HOME}${x2:1}"
#     if [[ "${x2}" == "${the_new_dir}" ]]; then
#       popd -n +$cnt 2>/dev/null 1>/dev/null
#       cnt=cnt-1
#     fi
#   done
# 
#   return 0
# }
# 
# alias cd=cd_func

export LANG=zh_CN.UTF-8
export LANGUAGE=zh_CN:en_US:en
export LC_CTYPE=zh_CN.UTF-8

export MSYSTEM=MINGW64
# export MSYS=winsymlinks:nativestrict
export PKG_CONFIG_PATH=/mingw64/lib/pkgconfig

# eval "$(dircolors -b /etc/DIR_COLORS)"

# if which ruby >/dev/null && which gem >/dev/null; then
# 	PATH="$PATH:$(ruby -rubygems -e 'puts Gem.user_dir')/bin"
# fi

# export PATH="$PATH:/mingw64/local/bin"
# export GOROOT="/mingw64"
# case $TERM in
#   (xterm*) set_title='\[\e]0;\u@\h: \w\a\]';;
#   (*) set_title=
# esac
# PS1=$set_title"\[\e[0;36m\]\T \[\e[1;30m\]\[\e[0;34m\]\u@\H\[\e[1;30m\] \[\e[0;32m\]\[\e[1;37m\]\w\[\e[0;37m\]\n\$ "

# if [ "${EMACS}" ]; then
# 	set_title=;
# else
# 	set_title='\[\e]0;\u@\h: \w\a\]';
# fi
# PS1=${set_title}'\[\e[0;36m\]\T \[\e[1;30m\]\[\e[0;34m\]\u@\H\[\e[1;30m\] \[\e[0;32m\]\[\e[1;37m\]\w\[\e[0;37m\]\n\$ '
# PROMPT_COMMAND='echo -en "\e[0;32m"$(( $(sed -nu "s/MemFree:[\t ]\+\([0-9]\+\) kB/\1/p" /proc/meminfo)/1024))"/"$(($(sed -nu "s/MemTotal:[\t ]\+\([0-9]\+\) kB/\1/Ip" /proc/meminfo)/1024 ))MB"\t\e[0;35m$(< /proc/loadavg)\e[0m"'
# PROMPT_COMMAND='echo -en "\e[0;32m"$(( $(sed -nu "s/MemFree:[\t ]\+\([0-9]\+\) kB/\1/p" /proc/meminfo)/1024))"/"$(($(sed -nu "s/MemTotal:[\t ]\+\([0-9]\+\) kB/\1/Ip" /proc/meminfo)/1024 ))MB"\t\e[0;35m$(< /proc/loadavg)"'
# PS1='\n\e[1;30m[$$:$PPID \j:\!\e[1;30m]\e[0;36m \T \d \e[1;30m[\e[1;34m\u@\H\e[1;30m] \e[1;37m\w `if [[ $? == 0 ]]; then echo -e "\e[01;32m\342\234\223"; else echo -e "\e[01;31m\342\234\227"; fi`\e[0m\n\$ '

export HISTFILESIZE=400000000
export HISTSIZE=10000000

export HISTCONTROL=ignoreboth:erasedups

# export PROMPT_COMMAND='history -a; echo -en "\e[m\e[38;5;2m"$(( $(sed -nu "s/MemFree:[\t ]\+\([0-9]\+\) kB/\1/p" /proc/meminfo)/1024))"\e[0;32m/"$(($(sed -nu "s/MemTotal:[\t ]\+\([0-9]\+\) kB/\1/Ip" /proc/meminfo)/1024 ))MB"\t\e[0;35m$(< /proc/loadavg)\e[m\n"'
export PROMPT_COMMAND='history -a; echo -en "\e[m\e[0;96m"$(( $(sed -nu "s/MemFree:[\t ]\+\([0-9]\+\) kB/\1/p" /proc/meminfo)/1024))"\e[0;32m/"$(($(sed -nu "s/MemTotal:[\t ]\+\([0-9]\+\) kB/\1/Ip" /proc/meminfo)/1024 ))MB"\t\e[0;35m$(< /proc/loadavg)\e[m\n"'
export PS1='\e[1;33m\t \d \e[1;35m[\e[1;34m\u@\H\e[1;35m] \e[1;37m\w `if [[ $? == 0 ]]; then echo -e "\e[1;32m\342\234\223"; else echo -e "\e[1;31m\342\234\227"; fi`\e[0m\n\$ '

# PROMPT_COMMAND='echo -en "\e[m\e[38;5;2m"$(( $(sed -nu "s/MemFree:[\t ]\+\([0-9]\+\) kB/\1/p" /proc/meminfo)/1024))"\e[0;32m/"$(($(sed -nu "s/MemTotal:[\t ]\+\([0-9]\+\) kB/\1/Ip" /proc/meminfo)/1024 ))MB"\t\e[0;35m$(< /proc/loadavg)\e[m"'
# PS1='\e[m\n\e[1;33m\t \d \e[1;35m[\e[1;34m\u@\H\e[1;35m] \e[1;37m\w `if [[ $? == 0 ]]; then echo -e "\e[01;32m\342\234\223"; else echo -e "\e[01;31m\342\234\227"; fi`\e[0m\n\$ '

# For Pacman.
alias pSyu='pacman -Syu'
alias pSu='pacman -Su'
alias pSyy='pacman -Syy'
alias pSs='pacman -Ss'

alias e='/mingw64/bin/emacsclient -c -n -a "" $@ &'
# alias an='/mingw64/bin/mpv "http://anonradio.net:8000/anonradio"'

an ()
{
    while : ; do
        "/mingw64/bin/mpv.exe" "--stream-lavf-o=reconnect_streamed=1" "http://anonradio.net:8000/anonradio"
        # [[ $? == 0 ]] && break
	sleep 5
    done
}

eval "$(dircolors -b /etc/DIR_COLORS)"

export PATH="$PATH:~/.emacs.d/.bin"

set_vina() {
	export v="/E/Vina/vina.exe"
}

opt_xyz() {
	declare M="/C/Program Files/MOPAC/MOPAC2016.exe"
	declare T="$(mktemp)"
	[[ ! -z "$1" ]] && [[ -e "$1" ]] && {
		cat << EOF > "${T}"
PM6-D3H4 GNORM=0.100 PRECISE PDBOUT
a

EOF
		awk 'NR>=3' "$1" >> "${T}"
		"${M}" "${T}"
		obabel "${T}.pdb" -O "opt-$1"
	}
}

opt_smi() {
	declare M="/C/Program Files/MOPAC/MOPAC2016.exe"
	declare T="$(mktemp -u)"
	[[ ! -z "$1" ]] && {
		obabel -:"$1" -O "${T}.mop" --gen3d -xk "PM6-D3H4 GNORM=0.100 PRECISE PDBOUT"
		"${M}" "${T}.mop"
		obabel "${T}.pdb" -O "opt.xyz"
	}
}

run_orca() {
	declare orca="/y/orca411/orca"
        [[ -z "$1" ]] && {
		[[ -e "a.inp" ]] && 
			nohup "${orca}" "$(realpath "a.inp")" &>> a.out &
			# echo 无参数，有a.inp
	}
	[[ ! -z "$1" ]] && {
		[[ -e "$1" ]] &&
			nohup ${orca} "$(realpath "$1")" &>> a.out &
			# echo 有参数，有文件
	}
}

trim() {
	filename="$1"
	extension="${filename##*.}"
	filename="${filename%.*}"
	[[ ! -z "$1" ]] && [[ ! -z "$2" ]] && {
		convert "$1" -units PixelsPerInch -density "$2" -trim "${filename}""-trim.png" &&
			optipng "${filename}""-trim.png"
		return
	}
	[[ ! -z "$1" ]] && {
		convert "$1" -units PixelsPerInch -density 600 -trim "${filename}""-trim.png" &&
			optipng "${filename}""-trim.png"
		return
	}
}

backupz() {
	cd /z;
	tar -I "zstd -T0" -cvf /Y/"$(date "+%Y%m%d")".tar.zst --exclude="System Volume Information" --exclude="\$RECYCLE.BIN" --exclude="*.tmp" ./
}

qr() {
	pushd ~/repo/hexo-blog/node_modules/qr-image
	node <<EOF
var qr = require('qr-image');
var qr_png = qr.image('$1', {type: 'png', size: 16, margin: 0});
qr_png.pipe(require('fs').createWriteStream('url.png'));
EOF
	mv url.png /z/url.png
	popd
}

loc() {
	multiwfn="/c/Multiwfn_3.6_dev_bin_Win64/Multiwfn"
	filename="$1"
	# extension="${filename##*.}"
	filename="${filename%.*}"

	"${multiwfn}" <<EOF
"$1"
19
1
100
2
6
${filename}-loc.molden
q
	
EOF

}

grepf() {
	grep "$1" -r . --include="*.$2" --color=always "${@:3}"
}
