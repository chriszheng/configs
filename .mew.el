(autoload 'mew "mew" nil t)
(autoload 'mew-send "mew" nil t)

;; (setq mew-prog-ssl "E:/msys/opt/bin/stunnel/stunnel.exe")
(setq mew-prog-ssl "C:/stunnel/bin/tstunnel.exe")
;; (setq mew-prog-ssl "E:/stunnel/tstunnel.exe")
(setq mew-ssl-verify-level 0)
(setq mew-use-cached-passwd t)
;; (setq mew-use-master-passwd t)
(setq mew-passwd-timer-unit 65536)

(setq mew-name "Chris Zheng")
(setq mew-user "chriszheng99")
(setq mew-mail-domain "gmail.com")

(setq mew-pop-user "chriszheng99@gmail.com")
(setq mew-pop-auth 'pass)
(setq mew-pop-ssl t)
(setq mew-pop-delete nil)
;; (setq mew-pop-server "64.233.176.108")
;; (setq mew-pop-ssl-port "995")
(setq mew-pop-server "127.0.01")
(setq mew-pop-ssl-port "995")

(setq mew-smtp-user "chriszheng99@gmail.com")
(setq mew-smtp-ssl t)
;; (setq mew-smtp-server "64.233.176.108")
;; (setq mew-smtp-ssl-port "465")
(setq mew-smtp-server "127.0.01")
(setq mew-smtp-ssl-port "465")

(eval-after-load 'mew-func
  '(progn
     (defun mew-time-ctz-to-rfc (time)
       (let* ((system-time-locale "C")
	      (date (format-time-string "%a, %d %b %Y %T %z" time)))
	 date
	 ))))

(setq mew-refile-guess-alist
      '(("To:"
         ("emacs-devel@gnu.org"	. "+inbox/emacs-dev")
         ("help-gnu-emacs@gnu.org"	. "+inbox/help-emacs"))
        ("Cc:"
         ("emacs-devel@gnu.org"	. "+inbox/emacs-dev")
         ("help-gnu-emacs@gnu.org"	. "+inbox/help-emacs"))
        (nil . "+inbox")))

(setq mew-refile-guess-control
      '(mew-refile-guess-by-folder
        mew-refile-guess-by-alist))

(when (and (fboundp 'shr-render-region)
           ;; \\[shr-render-region] requires Emacs to be compiled with libxml2.
           (fboundp 'libxml-parse-html-region))
  (setq mew-prog-text/html 'shr-render-region)) ;; 'mew-mime-text/html-w3m

(defun mew-ssl-options (case server remoteport localport tls)
  (setq server (mew-ssl-server server))
  (if (= mew-ssl-ver 3)
      (let (args)
	(setq args
	      `("-c" "-f"
		"-a" ,(expand-file-name (mew-ssl-cert-directory case))
		"-d" ,(format "%s:%d" mew-ssl-localhost localport)
		"-v" ,(number-to-string (mew-ssl-verify-level case))
		"-D" "debug"
		"-P" ""
		"-r" ,(format "%s:%d" server remoteport)
		,@mew-prog-ssl-arg))
	(if tls (setq args (cons "-n" (cons tls args))))
	args)
    (let ((file (mew-make-temp-name)))
      (with-temp-buffer
	(insert "client=yes\n")
	;; (insert "pid=\n")
	(insert (format "verify=%d\n" (mew-ssl-verify-level case)))
	;; (insert "foreground=yes\n")
	(insert "debug=debug\n")
	(if (and mew-ssl-libwrap (or (>= mew-ssl-ver 5) (>= mew-ssl-minor-ver 45)))
	    (insert "libwrap=no\n"))
	;; (if (or (>= mew-ssl-ver 5) (>= mew-ssl-minor-ver 22))
	;;     (insert "syslog=no\n"))
	(insert "CApath=" (expand-file-name (mew-ssl-cert-directory case)) "\n")
	(if mew-prog-ssl-arg
	    (insert mew-prog-ssl-arg))
	(insert (format "[%d]\n" localport))
	(insert (format "accept=%s:%d\n" mew-ssl-localhost localport))
	(insert (format "connect=%s:%d\n" server remoteport))
	(if tls (insert (format "protocol=%s\nsslVersion=TLSv1\n" tls)))
	(mew-frwlet mew-cs-dummy mew-cs-text-for-write
	  ;; NEVER use call-process-region for privacy reasons
	  (write-region (point-min) (point-max) file nil 'no-msg))
	(list file)))))
