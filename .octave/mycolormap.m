## Copyright (C) 2018-2019 Chris Zheng
##
## This file is not part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

function map = mycolormap (n)

  if (nargin > 1)
    print_usage ();
  elseif (nargin == 1)
    if (! isscalar (n))
      error ("mycolormap: N must be a scalar");
    endif
    n = double (n);
  else
    hf = get (0, "currentfigure");
    if (! isempty (hf))
      n = rows (get (hf, "colormap"));
    else
      n = 799;
    endif
  endif
  ## FIXME: Is there no algorithmic definition of the viridis colormap?
  persistent mycolormap = [[81 21 32] ./ 255;
			   [32 69 36] ./ 255;
			   [4 95 93] ./ 255;
			   [77 59 52] ./ 255;
			   [29 8 26] ./ 255;
			   [95 117 25] ./ 255;
			   [36 78 113] ./ 255;
			   [61 127 59] ./ 255;
			   [1 107 47] ./ 255;
			   [22 62 55] ./ 255;
			   [38 16 77] ./ 255;
			   [71 49 18] ./ 255;
			   [107 53 114] ./ 255;
			   [73 3 46] ./ 255;
			   [125 33 54] ./ 255;
			   [24 1 43] ./ 255;
			   [50 38 122] ./ 255;
			   [36 99 122] ./ 255;
			   [95 100 102] ./ 255;
			   [14 122 37] ./ 255;
			   [70 33 53] ./ 255;
			   [19 104 103] ./ 255;
			   [38 84 29] ./ 255;
			   [24 29 32] ./ 255;
			   [70 27 66] ./ 255;
			   [125 52 67] ./ 255;
			   [40 103 105] ./ 255;
			   [35 11 77] ./ 255;
			   [29 106 49] ./ 255;
			   [3 120 44] ./ 255;
			   [40 62 78] ./ 255;
			   [94 82 54] ./ 255;
			   [69 120 11] ./ 255;
			   [98 16 41] ./ 255;
			   [3 87 69] ./ 255;
			   [70 84 122] ./ 255;
			   [9 125 97] ./ 255;
			   [115 32 108] ./ 255;
			   [64 62 86] ./ 255;
			   [114 65 79] ./ 255;
			   [31 106 13] ./ 255;
			   [109 72 95] ./ 255;
			   [36 14 88] ./ 255;
			   [48 113 105] ./ 255;
			   [89 116 64] ./ 255;
			   [31 58 21] ./ 255;
			   [25 68 19] ./ 255;
			   [122 55 52] ./ 255;
			   [103 120 114] ./ 255;
			   [61 107 52] ./ 255;
			   [12 10 30] ./ 255;
			   [26 120 102] ./ 255;
			   [122 28 116] ./ 255;
			   [82 76 101] ./ 255;
			   [59 38 90] ./ 255;
			   [124 69 21] ./ 255;
			   [18 94 90] ./ 255;
			   [37 89 17] ./ 255;
			   [89 64 10] ./ 255;
			   [76 126 118] ./ 255;
			   [0 11 0] ./ 255;
			   [30 37 121] ./ 255;
			   [4 32 22] ./ 255;
			   [121 114 99] ./ 255;
			   [95 46 9] ./ 255;
			   [58 43 79] ./ 255;
			   [79 61 46] ./ 255;
			   [41 98 7] ./ 255;
			   [59 60 71] ./ 255;
			   [70 8 70] ./ 255;
			   [60 8 81] ./ 255;
			   [61 38 118] ./ 255;
			   [54 43 22] ./ 255;
			   [77 37 9] ./ 255;
			   [48 4 56] ./ 255;
			   [57 62 99] ./ 255;
			   [9 14 32] ./ 255;
			   [55 56 3] ./ 255;
			   [62 115 63] ./ 255;
			   [6 58 71] ./ 255;
			   [76 118 80] ./ 255;
			   [29 52 118] ./ 255;
			   [20 107 34] ./ 255;
			   [43 56 71] ./ 255;
			   [52 104 76] ./ 255;
			   [109 34 11] ./ 255;
			   [80 43 25] ./ 255;
			   [112 98 82] ./ 255;
			   [116 32 70] ./ 255;
			   [51 39 0] ./ 255;
			   [123 115 119] ./ 255;
			   [75 17 43] ./ 255;
			   [66 37 22] ./ 255;
			   [101 81 78] ./ 255;
			   [44 6 55] ./ 255;
			   [121 115 89] ./ 255;
			   [4 67 4] ./ 255;
			   [30 52 102] ./ 255;
			   [112 40 7] ./ 255;
			   [182 92 47] ./ 255;
			   [183 87 35] ./ 255;
			   [174 35 52] ./ 255;
			   [218 102 90] ./ 255;
			   [240 75 43] ./ 255;
			   [191 120 49] ./ 255;
			   [246 113 36] ./ 255;
			   [207 118 104] ./ 255;
			   [212 20 29] ./ 255;
			   [187 5 69] ./ 255;
			   [195 59 33] ./ 255;
			   [242 114 121] ./ 255;
			   [149 33 29] ./ 255;
			   [202 123 3] ./ 255;
			   [165 107 78] ./ 255;
			   [209 43 70] ./ 255;
			   [130 34 56] ./ 255;
			   [167 114 46] ./ 255;
			   [144 70 66] ./ 255;
			   [173 2 71] ./ 255;
			   [243 69 3] ./ 255;
			   [148 56 118] ./ 255;
			   [142 78 23] ./ 255;
			   [171 24 18] ./ 255;
			   [174 62 126] ./ 255;
			   [253 15 42] ./ 255;
			   [196 17 76] ./ 255;
			   [252 57 62] ./ 255;
			   [170 74 5] ./ 255;
			   [236 119 7] ./ 255;
			   [180 106 77] ./ 255;
			   [184 127 5] ./ 255;
			   [174 14 83] ./ 255;
			   [197 57 108] ./ 255;
			   [216 104 42] ./ 255;
			   [214 102 57] ./ 255;
			   [128 42 75] ./ 255;
			   [205 38 5] ./ 255;
			   [139 80 79] ./ 255;
			   [144 61 71] ./ 255;
			   [152 114 49] ./ 255;
			   [229 42 49] ./ 255;
			   [235 88 63] ./ 255;
			   [191 30 121] ./ 255;
			   [171 118 98] ./ 255;
			   [214 76 72] ./ 255;
			   [144 77 114] ./ 255;
			   [219 26 24] ./ 255;
			   [225 37 105] ./ 255;
			   [176 54 38] ./ 255;
			   [248 79 25] ./ 255;
			   [169 53 67] ./ 255;
			   [219 32 28] ./ 255;
			   [154 95 58] ./ 255;
			   [148 11 48] ./ 255;
			   [246 97 124] ./ 255;
			   [190 113 74] ./ 255;
			   [177 77 100] ./ 255;
			   [202 47 10] ./ 255;
			   [179 96 65] ./ 255;
			   [218 88 16] ./ 255;
			   [243 1 69] ./ 255;
			   [183 93 101] ./ 255;
			   [211 120 69] ./ 255;
			   [141 12 80] ./ 255;
			   [189 2 50] ./ 255;
			   [186 65 36] ./ 255;
			   [132 115 113] ./ 255;
			   [233 61 33] ./ 255;
			   [243 113 1] ./ 255;
			   [180 75 89] ./ 255;
			   [197 63 91] ./ 255;
			   [138 118 56] ./ 255;
			   [240 73 48] ./ 255;
			   [181 87 60] ./ 255;
			   [134 21 63] ./ 255;
			   [184 79 1] ./ 255;
			   [220 84 117] ./ 255;
			   [206 61 50] ./ 255;
			   [239 48 35] ./ 255;
			   [241 101 111] ./ 255;
			   [202 42 46] ./ 255;
			   [165 53 36] ./ 255;
			   [222 37 110] ./ 255;
			   [142 91 70] ./ 255;
			   [203 98 91] ./ 255;
			   [139 26 42] ./ 255;
			   [141 119 126] ./ 255;
			   [130 70 60] ./ 255;
			   [180 54 108] ./ 255;
			   [216 39 82] ./ 255;
			   [199 113 125] ./ 255;
			   [246 23 50] ./ 255;
			   [155 117 88] ./ 255;
			   [137 4 51] ./ 255;
			   [207 79 21] ./ 255;
			   [171 91 48] ./ 255;
			   [213 104 40] ./ 255;
			   [212 106 111] ./ 255;
			   [144 31 37] ./ 255;
			   [125 248 76] ./ 255;
			   [80 192 62] ./ 255;
			   [77 182 86] ./ 255;
			   [127 209 76] ./ 255;
			   [87 219 80] ./ 255;
			   [11 171 32] ./ 255;
			   [33 214 123] ./ 255;
			   [81 171 99] ./ 255;
			   [122 128 78] ./ 255;
			   [105 145 110] ./ 255;
			   [14 142 102] ./ 255;
			   [91 223 38] ./ 255;
			   [26 172 92] ./ 255;
			   [113 171 46] ./ 255;
			   [61 131 9] ./ 255;
			   [13 142 52] ./ 255;
			   [46 175 10] ./ 255;
			   [41 129 54] ./ 255;
			   [13 251 54] ./ 255;
			   [92 228 71] ./ 255;
			   [74 243 86] ./ 255;
			   [49 207 53] ./ 255;
			   [87 233 98] ./ 255;
			   [52 218 13] ./ 255;
			   [98 151 17] ./ 255;
			   [108 165 31] ./ 255;
			   [33 211 79] ./ 255;
			   [43 253 80] ./ 255;
			   [97 139 75] ./ 255;
			   [24 232 48] ./ 255;
			   [96 178 36] ./ 255;
			   [55 227 115] ./ 255;
			   [108 187 92] ./ 255;
			   [79 240 55] ./ 255;
			   [92 211 79] ./ 255;
			   [110 191 117] ./ 255;
			   [13 224 73] ./ 255;
			   [92 140 70] ./ 255;
			   [45 238 82] ./ 255;
			   [120 134 58] ./ 255;
			   [41 231 109] ./ 255;
			   [77 158 80] ./ 255;
			   [64 139 12] ./ 255;
			   [29 218 124] ./ 255;
			   [84 183 79] ./ 255;
			   [36 165 15] ./ 255;
			   [25 178 111] ./ 255;
			   [98 143 124] ./ 255;
			   [41 188 106] ./ 255;
			   [123 181 113] ./ 255;
			   [54 223 88] ./ 255;
			   [35 172 119] ./ 255;
			   [116 237 2] ./ 255;
			   [0 139 93] ./ 255;
			   [125 223 20] ./ 255;
			   [76 132 57] ./ 255;
			   [92 157 108] ./ 255;
			   [76 128 124] ./ 255;
			   [72 169 57] ./ 255;
			   [50 165 110] ./ 255;
			   [35 219 78] ./ 255;
			   [124 254 123] ./ 255;
			   [115 242 104] ./ 255;
			   [118 242 115] ./ 255;
			   [83 239 83] ./ 255;
			   [103 188 87] ./ 255;
			   [33 152 117] ./ 255;
			   [13 229 117] ./ 255;
			   [10 173 31] ./ 255;
			   [67 224 68] ./ 255;
			   [50 131 31] ./ 255;
			   [0 128 29] ./ 255;
			   [123 244 15] ./ 255;
			   [100 234 2] ./ 255;
			   [88 189 114] ./ 255;
			   [43 164 47] ./ 255;
			   [3 198 72] ./ 255;
			   [121 212 45] ./ 255;
			   [110 222 90] ./ 255;
			   [13 162 58] ./ 255;
			   [81 212 62] ./ 255;
			   [113 213 62] ./ 255;
			   [14 209 50] ./ 255;
			   [30 182 29] ./ 255;
			   [33 142 90] ./ 255;
			   [19 185 127] ./ 255;
			   [66 189 69] ./ 255;
			   [10 182 25] ./ 255;
			   [55 165 119] ./ 255;
			   [18 178 26] ./ 255;
			   [76 132 110] ./ 255;
			   [11 245 68] ./ 255;
			   [74 132 22] ./ 255;
			   [125 162 76] ./ 255;
			   [26 195 90] ./ 255;
			   [116 215 20] ./ 255;
			   [115 154 81] ./ 255;
			   [57 165 8] ./ 255;
			   [82 221 45] ./ 255;
			   [74 239 95] ./ 255;
			   [228 188 100] ./ 255;
			   [211 199 90] ./ 255;
			   [152 145 94] ./ 255;
			   [174 142 1] ./ 255;
			   [250 168 69] ./ 255;
			   [212 157 29] ./ 255;
			   [233 145 55] ./ 255;
			   [186 202 92] ./ 255;
			   [194 156 57] ./ 255;
			   [239 231 40] ./ 255;
			   [207 204 101] ./ 255;
			   [180 159 44] ./ 255;
			   [142 184 62] ./ 255;
			   [237 230 77] ./ 255;
			   [238 224 118] ./ 255;
			   [180 181 19] ./ 255;
			   [209 158 36] ./ 255;
			   [137 217 111] ./ 255;
			   [229 156 12] ./ 255;
			   [159 140 115] ./ 255;
			   [200 219 63] ./ 255;
			   [173 143 95] ./ 255;
			   [217 158 23] ./ 255;
			   [152 139 126] ./ 255;
			   [229 250 95] ./ 255;
			   [219 174 20] ./ 255;
			   [238 128 51] ./ 255;
			   [147 137 13] ./ 255;
			   [130 239 41] ./ 255;
			   [142 142 53] ./ 255;
			   [130 214 17] ./ 255;
			   [194 131 33] ./ 255;
			   [162 221 63] ./ 255;
			   [185 245 75] ./ 255;
			   [184 219 69] ./ 255;
			   [151 182 116] ./ 255;
			   [172 165 117] ./ 255;
			   [223 184 126] ./ 255;
			   [236 187 109] ./ 255;
			   [150 202 123] ./ 255;
			   [203 204 82] ./ 255;
			   [220 143 85] ./ 255;
			   [253 177 51] ./ 255;
			   [189 235 40] ./ 255;
			   [136 163 3] ./ 255;
			   [206 186 58] ./ 255;
			   [194 230 95] ./ 255;
			   [183 198 24] ./ 255;
			   [182 178 84] ./ 255;
			   [164 200 30] ./ 255;
			   [160 148 107] ./ 255;
			   [242 241 123] ./ 255;
			   [200 238 44] ./ 255;
			   [251 172 23] ./ 255;
			   [164 180 59] ./ 255;
			   [167 131 117] ./ 255;
			   [226 198 92] ./ 255;
			   [194 254 34] ./ 255;
			   [219 180 85] ./ 255;
			   [175 216 30] ./ 255;
			   [206 249 51] ./ 255;
			   [186 235 36] ./ 255;
			   [181 179 19] ./ 255;
			   [226 174 63] ./ 255;
			   [250 210 116] ./ 255;
			   [181 250 119] ./ 255;
			   [171 221 61] ./ 255;
			   [135 159 59] ./ 255;
			   [170 251 112] ./ 255;
			   [128 170 73] ./ 255;
			   [158 249 66] ./ 255;
			   [209 180 46] ./ 255;
			   [246 234 98] ./ 255;
			   [137 204 17] ./ 255;
			   [200 198 99] ./ 255;
			   [189 251 94] ./ 255;
			   [180 167 60] ./ 255;
			   [242 174 92] ./ 255;
			   [174 217 87] ./ 255;
			   [159 217 2] ./ 255;
			   [233 247 123] ./ 255;
			   [171 201 48] ./ 255;
			   [218 191 26] ./ 255;
			   [188 201 102] ./ 255;
			   [205 145 45] ./ 255;
			   [177 206 41] ./ 255;
			   [144 131 80] ./ 255;
			   [204 246 127] ./ 255;
			   [168 165 88] ./ 255;
			   [128 196 49] ./ 255;
			   [130 173 41] ./ 255;
			   [253 217 115] ./ 255;
			   [174 180 51] ./ 255;
			   [200 240 124] ./ 255;
			   [175 189 13] ./ 255;
			   [220 239 92] ./ 255;
			   [134 255 96] ./ 255;
			   [215 204 87] ./ 255;
			   [214 245 125] ./ 255;
			   [175 245 66] ./ 255;
			   [96 119 239] ./ 255;
			   [10 117 201] ./ 255;
			   [125 35 253] ./ 255;
			   [49 108 238] ./ 255;
			   [45 27 172] ./ 255;
			   [59 120 155] ./ 255;
			   [24 126 155] ./ 255;
			   [121 86 231] ./ 255;
			   [80 44 221] ./ 255;
			   [77 91 210] ./ 255;
			   [15 60 202] ./ 255;
			   [127 71 192] ./ 255;
			   [73 69 227] ./ 255;
			   [71 118 208] ./ 255;
			   [53 35 235] ./ 255;
			   [98 94 227] ./ 255;
			   [125 119 226] ./ 255;
			   [25 112 184] ./ 255;
			   [1 65 229] ./ 255;
			   [94 14 193] ./ 255;
			   [49 30 253] ./ 255;
			   [123 30 197] ./ 255;
			   [59 104 138] ./ 255;
			   [31 48 128] ./ 255;
			   [111 102 163] ./ 255;
			   [91 72 130] ./ 255;
			   [63 70 249] ./ 255;
			   [34 95 233] ./ 255;
			   [90 96 170] ./ 255;
			   [64 62 185] ./ 255;
			   [1 111 216] ./ 255;
			   [127 107 247] ./ 255;
			   [68 38 224] ./ 255;
			   [79 70 144] ./ 255;
			   [79 54 246] ./ 255;
			   [115 17 191] ./ 255;
			   [118 80 133] ./ 255;
			   [111 114 228] ./ 255;
			   [89 77 196] ./ 255;
			   [4 14 131] ./ 255;
			   [62 15 243] ./ 255;
			   [23 15 222] ./ 255;
			   [15 84 133] ./ 255;
			   [111 35 203] ./ 255;
			   [0 115 129] ./ 255;
			   [118 102 147] ./ 255;
			   [54 92 228] ./ 255;
			   [59 76 215] ./ 255;
			   [32 38 164] ./ 255;
			   [100 43 178] ./ 255;
			   [104 105 194] ./ 255;
			   [91 0 210] ./ 255;
			   [57 15 166] ./ 255;
			   [63 127 202] ./ 255;
			   [10 127 189] ./ 255;
			   [12 118 164] ./ 255;
			   [32 44 128] ./ 255;
			   [4 104 205] ./ 255;
			   [91 8 244] ./ 255;
			   [127 109 159] ./ 255;
			   [50 85 137] ./ 255;
			   [117 49 137] ./ 255;
			   [71 107 153] ./ 255;
			   [109 42 153] ./ 255;
			   [56 53 152] ./ 255;
			   [117 65 143] ./ 255;
			   [25 97 188] ./ 255;
			   [26 102 164] ./ 255;
			   [104 65 172] ./ 255;
			   [92 65 153] ./ 255;
			   [123 116 239] ./ 255;
			   [4 105 160] ./ 255;
			   [14 49 139] ./ 255;
			   [40 31 182] ./ 255;
			   [66 87 235] ./ 255;
			   [91 76 173] ./ 255;
			   [106 101 143] ./ 255;
			   [38 0 245] ./ 255;
			   [75 104 182] ./ 255;
			   [119 68 248] ./ 255;
			   [17 64 236] ./ 255;
			   [0 69 213] ./ 255;
			   [33 83 134] ./ 255;
			   [45 124 165] ./ 255;
			   [99 62 253] ./ 255;
			   [78 25 201] ./ 255;
			   [124 4 175] ./ 255;
			   [11 43 175] ./ 255;
			   [0 118 151] ./ 255;
			   [55 109 220] ./ 255;
			   [47 126 156] ./ 255;
			   [27 127 225] ./ 255;
			   [113 32 181] ./ 255;
			   [120 77 178] ./ 255;
			   [30 48 240] ./ 255;
			   [27 127 138] ./ 255;
			   [101 123 142] ./ 255;
			   [20 7 185] ./ 255;
			   [68 7 175] ./ 255;
			   [92 63 157] ./ 255;
			   [184 110 156] ./ 255;
			   [213 10 155] ./ 255;
			   [183 124 188] ./ 255;
			   [237 117 137] ./ 255;
			   [159 19 186] ./ 255;
			   [144 47 185] ./ 255;
			   [154 20 181] ./ 255;
			   [169 41 188] ./ 255;
			   [227 109 196] ./ 255;
			   [147 73 132] ./ 255;
			   [176 2 243] ./ 255;
			   [205 87 253] ./ 255;
			   [232 14 250] ./ 255;
			   [164 123 239] ./ 255;
			   [174 27 131] ./ 255;
			   [232 43 178] ./ 255;
			   [162 70 199] ./ 255;
			   [216 111 240] ./ 255;
			   [149 83 222] ./ 255;
			   [218 102 167] ./ 255;
			   [222 23 169] ./ 255;
			   [209 100 129] ./ 255;
			   [207 77 143] ./ 255;
			   [201 114 139] ./ 255;
			   [185 32 166] ./ 255;
			   [188 9 210] ./ 255;
			   [239 44 152] ./ 255;
			   [183 4 136] ./ 255;
			   [168 26 219] ./ 255;
			   [134 116 194] ./ 255;
			   [174 83 217] ./ 255;
			   [215 36 190] ./ 255;
			   [216 116 140] ./ 255;
			   [232 61 255] ./ 255;
			   [244 119 160] ./ 255;
			   [154 52 169] ./ 255;
			   [236 35 214] ./ 255;
			   [133 91 218] ./ 255;
			   [141 3 245] ./ 255;
			   [232 9 233] ./ 255;
			   [171 55 188] ./ 255;
			   [133 15 225] ./ 255;
			   [196 104 213] ./ 255;
			   [208 81 147] ./ 255;
			   [207 69 138] ./ 255;
			   [239 96 190] ./ 255;
			   [153 76 226] ./ 255;
			   [239 82 189] ./ 255;
			   [202 95 193] ./ 255;
			   [191 72 203] ./ 255;
			   [169 115 130] ./ 255;
			   [229 120 146] ./ 255;
			   [198 60 250] ./ 255;
			   [156 13 203] ./ 255;
			   [175 93 145] ./ 255;
			   [185 77 241] ./ 255;
			   [248 102 190] ./ 255;
			   [218 86 144] ./ 255;
			   [152 33 239] ./ 255;
			   [217 96 183] ./ 255;
			   [164 10 171] ./ 255;
			   [167 111 163] ./ 255;
			   [186 54 224] ./ 255;
			   [180 82 237] ./ 255;
			   [128 2 202] ./ 255;
			   [145 59 152] ./ 255;
			   [131 51 254] ./ 255;
			   [193 14 213] ./ 255;
			   [209 39 246] ./ 255;
			   [193 1 215] ./ 255;
			   [248 38 225] ./ 255;
			   [164 77 209] ./ 255;
			   [199 7 136] ./ 255;
			   [168 60 218] ./ 255;
			   [149 61 220] ./ 255;
			   [224 79 152] ./ 255;
			   [248 82 204] ./ 255;
			   [247 20 218] ./ 255;
			   [205 101 130] ./ 255;
			   [195 38 131] ./ 255;
			   [155 31 169] ./ 255;
			   [253 67 247] ./ 255;
			   [206 11 254] ./ 255;
			   [214 51 187] ./ 255;
			   [177 73 249] ./ 255;
			   [142 42 200] ./ 255;
			   [167 35 155] ./ 255;
			   [243 27 175] ./ 255;
			   [206 104 149] ./ 255;
			   [208 44 188] ./ 255;
			   [211 72 219] ./ 255;
			   [252 69 159] ./ 255;
			   [243 19 171] ./ 255;
			   [242 106 223] ./ 255;
			   [174 27 169] ./ 255;
			   [168 42 212] ./ 255;
			   [240 81 247] ./ 255;
			   [140 68 147] ./ 255;
			   [188 18 251] ./ 255;
			   [209 99 168] ./ 255;
			   [13 182 240] ./ 255;
			   [104 179 181] ./ 255;
			   [8 167 201] ./ 255;
			   [51 154 179] ./ 255;
			   [19 201 207] ./ 255;
			   [60 241 249] ./ 255;
			   [16 225 202] ./ 255;
			   [8 238 143] ./ 255;
			   [27 170 162] ./ 255;
			   [23 251 133] ./ 255;
			   [63 136 187] ./ 255;
			   [48 241 239] ./ 255;
			   [102 250 150] ./ 255;
			   [47 174 177] ./ 255;
			   [99 193 250] ./ 255;
			   [51 253 235] ./ 255;
			   [45 141 205] ./ 255;
			   [119 149 187] ./ 255;
			   [7 177 230] ./ 255;
			   [41 200 225] ./ 255;
			   [46 135 234] ./ 255;
			   [106 184 220] ./ 255;
			   [90 158 214] ./ 255;
			   [113 206 132] ./ 255;
			   [34 178 197] ./ 255;
			   [28 229 195] ./ 255;
			   [8 147 209] ./ 255;
			   [85 139 231] ./ 255;
			   [17 146 152] ./ 255;
			   [119 187 224] ./ 255;
			   [89 234 232] ./ 255;
			   [67 213 160] ./ 255;
			   [32 175 191] ./ 255;
			   [118 160 141] ./ 255;
			   [123 194 192] ./ 255;
			   [65 223 166] ./ 255;
			   [4 232 185] ./ 255;
			   [86 189 196] ./ 255;
			   [61 207 214] ./ 255;
			   [85 199 146] ./ 255;
			   [53 160 252] ./ 255;
			   [30 228 209] ./ 255;
			   [63 132 128] ./ 255;
			   [126 251 160] ./ 255;
			   [12 247 227] ./ 255;
			   [76 184 195] ./ 255;
			   [114 189 171] ./ 255;
			   [43 147 233] ./ 255;
			   [111 208 184] ./ 255;
			   [70 166 255] ./ 255;
			   [88 219 160] ./ 255;
			   [85 250 132] ./ 255;
			   [38 185 137] ./ 255;
			   [39 183 133] ./ 255;
			   [72 195 252] ./ 255;
			   [44 144 181] ./ 255;
			   [111 130 242] ./ 255;
			   [26 174 134] ./ 255;
			   [3 158 214] ./ 255;
			   [60 228 252] ./ 255;
			   [60 188 216] ./ 255;
			   [92 145 211] ./ 255;
			   [97 184 140] ./ 255;
			   [107 223 195] ./ 255;
			   [112 168 135] ./ 255;
			   [108 212 151] ./ 255;
			   [33 195 154] ./ 255;
			   [20 222 200] ./ 255;
			   [26 225 230] ./ 255;
			   [113 158 203] ./ 255;
			   [110 218 136] ./ 255;
			   [71 183 153] ./ 255;
			   [26 153 210] ./ 255;
			   [38 132 178] ./ 255;
			   [106 244 218] ./ 255;
			   [114 225 175] ./ 255;
			   [10 131 242] ./ 255;
			   [36 152 208] ./ 255;
			   [109 178 178] ./ 255;
			   [84 164 209] ./ 255;
			   [31 147 172] ./ 255;
			   [39 219 227] ./ 255;
			   [65 246 252] ./ 255;
			   [20 157 128] ./ 255;
			   [70 135 245] ./ 255;
			   [33 250 215] ./ 255;
			   [80 132 218] ./ 255;
			   [67 169 242] ./ 255;
			   [20 150 165] ./ 255;
			   [71 235 202] ./ 255;
			   [24 139 221] ./ 255;
			   [68 179 185] ./ 255;
			   [40 245 175] ./ 255;
			   [36 137 204] ./ 255;
			   [37 208 212] ./ 255;
			   [26 241 206] ./ 255;
			   [113 193 210] ./ 255;
			   [76 133 252] ./ 255;
			   [62 153 146] ./ 255;
			   [100 224 254] ./ 255;
			   [174 248 137] ./ 255;
			   [140 189 188] ./ 255;
			   [197 229 177] ./ 255;
			   [244 138 187] ./ 255;
			   [193 175 139] ./ 255;
			   [149 202 252] ./ 255;
			   [227 188 190] ./ 255;
			   [182 136 195] ./ 255;
			   [178 199 220] ./ 255;
			   [197 171 188] ./ 255;
			   [195 218 181] ./ 255;
			   [204 230 243] ./ 255;
			   [137 172 216] ./ 255;
			   [187 160 226] ./ 255;
			   [246 226 146] ./ 255;
			   [129 247 220] ./ 255;
			   [253 219 152] ./ 255;
			   [188 146 160] ./ 255;
			   [255 196 231] ./ 255;
			   [220 138 147] ./ 255;
			   [153 205 237] ./ 255;
			   [206 154 212] ./ 255;
			   [193 164 128] ./ 255;
			   [154 223 160] ./ 255;
			   [253 214 131] ./ 255;
			   [143 215 250] ./ 255;
			   [235 213 214] ./ 255;
			   [131 145 232] ./ 255;
			   [164 145 173] ./ 255;
			   [140 237 183] ./ 255;
			   [159 134 133] ./ 255;
			   [141 213 160] ./ 255;
			   [225 151 197] ./ 255;
			   [225 177 164] ./ 255;
			   [130 174 250] ./ 255;
			   [133 190 210] ./ 255;
			   [128 169 167] ./ 255;
			   [214 173 185] ./ 255;
			   [190 209 203] ./ 255;
			   [236 221 184] ./ 255;
			   [163 253 191] ./ 255;
			   [169 139 148] ./ 255;
			   [201 236 171] ./ 255;
			   [142 206 221] ./ 255;
			   [179 209 140] ./ 255;
			   [174 214 202] ./ 255;
			   [129 215 243] ./ 255;
			   [169 173 161] ./ 255;
			   [226 236 242] ./ 255;
			   [173 217 208] ./ 255;
			   [230 252 206] ./ 255;
			   [165 166 217] ./ 255;
			   [185 239 198] ./ 255;
			   [229 254 149] ./ 255;
			   [194 178 230] ./ 255;
			   [206 225 189] ./ 255;
			   [152 226 148] ./ 255;
			   [140 139 194] ./ 255;
			   [173 238 175] ./ 255;
			   [160 156 136] ./ 255;
			   [241 131 133] ./ 255;
			   [191 168 171] ./ 255;
			   [153 226 155] ./ 255;
			   [224 199 154] ./ 255;
			   [245 138 204] ./ 255;
			   [219 216 173] ./ 255;
			   [152 241 144] ./ 255;
			   [173 254 156] ./ 255;
			   [239 171 139] ./ 255;
			   [158 204 167] ./ 255;
			   [167 189 170] ./ 255;
			   [172 252 211] ./ 255;
			   [215 150 181] ./ 255;
			   [243 246 253] ./ 255;
			   [141 236 135] ./ 255;
			   [218 199 224] ./ 255;
			   [136 224 210] ./ 255;
			   [152 141 208] ./ 255;
			   [180 253 252] ./ 255;
			   [192 155 200] ./ 255;
			   [231 195 133] ./ 255;
			   [146 239 130] ./ 255;
			   [230 199 152] ./ 255;
			   [155 187 143] ./ 255;
			   [153 200 251] ./ 255;
			   [161 162 195] ./ 255;
			   [130 170 163] ./ 255;
			   [212 195 177] ./ 255;
			   [164 248 174] ./ 255;
			   [160 184 202] ./ 255;
			   [232 159 141] ./ 255;
			   [238 178 253] ./ 255;
			   [240 152 197] ./ 255;
			   [136 180 128] ./ 255;
			   [152 206 200] ./ 255;
			   [147 239 234] ./ 255;
			   [214 241 149] ./ 255;
			   [250 197 216] ./ 255;
			   [171 234 208] ./ 255;
			   [217 138 136] ./ 255;
			  ];

  p = rows (mycolormap);
  map = interp1 (1:p, mycolormap, linspace (1, p, n), "linear");

endfunction


## A better demo of this colormap would be to plot the CIECAM02 values.
%!demo
%! ## Show the 'mycolormap' colormap profile and as an image
%! cmap = mycolormap (256);
%! subplot (2, 1, 1);
%!  rgbplot (cmap, "composite");
%! subplot (2, 1, 2);
%!  rgbplot (cmap);
