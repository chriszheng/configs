## Copyright (C) 2018-2019 Chris Zheng
##
## This file is not part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

function map = ggplot2 (n)

  if (nargin > 1)
    print_usage ();
  elseif (nargin == 1)
    if (! isscalar (n))
      error ("mycolormap: N must be a scalar");
    endif
    n = double (n);
  else
    hf = get (0, "currentfigure");
    if (! isempty (hf))
      n = rows (get (hf, "colormap"));
    else
      n = 799;
    endif
  endif
  persistent ggplot2 = colorscale(n);

  p = rows (ggplot2);
  map = interp1 (1:p, ggplot2, linspace (1, p, n), "linear");

endfunction


## A better demo of this colormap would be to plot the CIECAM02 values.
%!demo
%! ## Show the 'mycolormap' colormap profile and as an image
%! cmap = ggplot2 (256);
%! subplot (2, 1, 1);
%!  rgbplot (cmap, "composite");
%! subplot (2, 1, 2);
%!  rgbplot (cmap);
