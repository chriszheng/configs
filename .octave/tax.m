function retval = tax (money)
  start = 3500;
  l = money .- start;
  l(l <= 0) = 0;

  retval = arrayfun(@p, l);
  ## if (money <= start + 1500)
  ##   retval = liability * 0.03;
  ##   return;
  ## endif

  ## if (money <= start + 4500)
  ##   retval = (money - start) * 0.1 - 105;
  ##   return;
  ## endif

  ## if (money <= start + 9000)
  ##   retval = (money - start -4500) * 0.2 + 555;
  ##   return;
  ## endif

  ## if (money <= start + 35000)
  ##   retval = (money - start - 9000) * 0.25 + 1005;
  ##   return;
  ## endif

  ## if (money <= start + 55000)
  ##   retval = (money - start - 35000) * 0.3 + 2755;
  ##   return;
  ## endif

  ## if (money <= start + 80000)
  ##   retval = (money - start - 55000) * 0.35 + 5505;
  ##   return;
  ## endif

  ## retval = (money - start - 80000) * 0.45 + 13505;
  
endfunction

function r = p (val)
  table = [1500   0.03      0
  	   4500   0.10    105 
  	   9000   0.20    555 
  	   35000  0.25   1005
  	   55000  0.30   2755
  	   80000  0.35   5505
  	   80000  0.45  13505
  	  ];	 
  for x = 1:size(table, 1)
    if (val <= table(x, 1))
      r = val * table(x, 2) - table(x, 3);
      return;
    endif
  endfor
  r = val * table(end, 2) - table(end, 3);
endfunction


## set(findall(gcf(), "-property", "interpreter"), "interpreter", "TeX");
## xlabel('\fontsize{24}{Money}');
## ylabel('\fontsize{36}{Tax}');
