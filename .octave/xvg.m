function xvg (xvgname)
  fid = fopen(xvgname);
  if (fid == -1)
    return;
  endif
  for i = 1:12
    fgetl(fid);
  endfor
  t = regexprep(fgetl(fid), '\S+\s+\S+\s+\"([^"]+)\"', '$1');
  xl = regexprep(fgetl(fid), '\S+\s+\S+\s+\S+\s+\"([^"]+)\"', '$1');
  yl = regexprep(fgetl(fid), '\S+\s+\S+\s+\S+\s+\"([^"]+)\"', '$1');
  for i = 1:8
    fgetl(fid);
  endfor
  d = fscanf(fid, "%g %g", [2, Inf]);
  plot(d(1, :), d(2, :));
  title(t);
  xlabel(xl);
  ylabel(yl);
  fclose(fid);
endfunction

