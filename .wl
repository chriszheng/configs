;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; wanderlust

;; (setq elmo-maildir-folder-path "~/Maildir")          ;; where i store my mail

;; SMTP
(setq wl-smtp-connection-type 'starttls
      wl-smtp-posting-port 587
      wl-smtp-authenticate-type "plain"
      wl-smtp-posting-user "chriszheng99"
      wl-smtp-posting-server "127.0.0.1"
      wl-local-domain "gmail.com"
      wl-message-id-domain "smtp.gmail.com")

(setq wl-from "Chris Zheng <chriszheng99@gmail.com>"
      ;; wl-default-folder "&\"Inbox:chriszheng99@gmail.com\"@127.0.0.1:995!"
      wl-draft-folder "+draft"
      wl-trash-folder "+trash"
      wl-fcc-force-as-read t
      wl-default-spec "&"
      wl-fcc "+sent")

(setq wl-subscribed-mailing-list
      '("help-gnu-emacs@gnu.org"
	"emacs-devel@gnu.org"))

(setq wl-message-ignored-field-list
      '("Received-SPF:"
	".*Received:"
	".*Path:"
	".*Id:"
	"^References:"
	"^Replied:"
	"^Errors-To:"
	"^Lines:"
	"^Sender:"
	".*Host:"
	"^Xref:"
	"^Content-.*:"
	"^Precedence:"
	"^Status:"
	"^X-VM-.*:"
	"^ARC-.*:"
	"^Authentication-Results:"
	"^X-NAI-Spam.*:"
	"X-.*:"
	"MIME-Version:"
	"List-.*:"
	"DKIM-Signature:"
	))

(setq wl-summary-default-number-column 0)
(setq wl-summary-width 100)

;; Refile rule
(setq wl-refile-rule-alist
      '(("To:"
         ("emacs-devel@gnu.org" . "+ml/emacs-devel")
         ("help-gnu-emacs@gnu.org" . "+ml/help-gnu-emacs"))
	("Cc:"
	 ("emacs-devel@gnu.org" . "+ml/emacs-devel")
         ("help-gnu-emacs@gnu.org" . "+ml/help-gnu-emacs"))))

;; Expire
(setq wl-expire-alist
      '(("^\\&" (date 7) trash)
        ))

(add-hook 'wl-summary-prepared-hook 'wl-summary-expire)

;; Truncate lines, i.e. do not show content outside the frame.
(setq wl-draft-truncate-lines nil)

(setq mime-edit-split-message nil)

(setq mime-view-text/html-previewer 'shr)

(setq elmo-passwd-alist-file-name "~/.mutt/.gmail.gpg")
